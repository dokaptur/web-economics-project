
# coding: utf-8


import numpy as np
import pandas as pd
import sklearn as sk

data_file = open('data/shuffle_data_train.txt', 'r')
headers = ['click', 'weekday', 'hour', 'timestamp', 'log_type', 'user_id', 'user_agent', 'ip', 'region', 'city',
           'ad_exchange', 'domain', 'url', 'anonymous_url_id', 'ad_slot_id', 'ad_slot_width', 'ad_slot_height',
           'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price', 'creative_id', 'key_page_url',
           'advertiser_id', 'user_tags']
data = pd.read_csv(data_file, delimiter='\t', names=headers)
data_file.close()

test_file = open('data/shuffle_data_test.txt', 'r')
test = pd.read_csv(test_file, delimiter='\t', names=headers[1:])

positives = data[data['click'] == 1]
train_ips = set(positives['ip'])
test_ip = set(test['ip'])

print('positive ips in train set: {}'.format(len(train_ips)))
print('ips in test set: {}'.format(len(test_ip)))
print('common ips: {}'.format(len(train_ips.intersection(test_ip))))

for h in headers:
    print('{}: {}, {}'.format(h, len(data[h].unique()), data[h].dtype))

print('\nnumber of examples: {}'.format(data.shape[0]))
print('number of ones: {}\n'.format(sum(data['click'])))

positives = data[data['click'] == 1]
print(positives.shape)
positives.to_csv('data/train_positives', index=False, header=False, sep='\t')


