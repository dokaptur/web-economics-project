import json
import numpy as np
import pandas as pd
from sklearn.ensemble import AdaBoostRegressor, RandomForestRegressor
from sklearn.metrics import roc_auc_score
from sklearn.cross_validation import StratifiedKFold, StratifiedShuffleSplit
from sklearn.tree import DecisionTreeRegressor
from sklearn.grid_search import GridSearchCV
from sklearn.svm import SVR
from sklearn.kernel_ridge import KernelRidge
from xgboost.sklearn import DMatrix, XGBClassifier


def load_json(filename):
    with open(filename, 'r') as f:
        X = json.load(f)
        return X


def load_data(filename_X, filename_Y):
    X = load_json(filename_X)
    Y = np.load(filename_Y)
    return X, Y


def save_submission(Yp, filename='data/tmp_submission.csv'):
    df = pd.read_csv('data/sample_submission.csv')
    df['Prediction'] = Yp
    df.to_csv(filename, index=False)


def learn_xgboost(X, Y):
    print('learning...')
    clf = XGBClassifier(n_estimators=100, silent=False, reg_alpha=0.2, reg_lambda=0.2, scale_pos_weight=2)
    sss = StratifiedShuffleSplit(Y, n_iter=1, test_size=0.2)
    for trainIdx, testIdx in sss:
        eval_set = [(X[trainIdx], Y[trainIdx]), (X[testIdx], Y[testIdx])]
        clf.fit(X[trainIdx], Y[trainIdx], verbose=True, eval_metric='auc', eval_set=eval_set, early_stopping_rounds=5)
        print('eval results:')
        print(clf.evals_result())
    return clf


def learn_svr(X, Y):
    print('learning...')
    clf = GridSearchCV(SVR(kernel='rbf'), cv=5, param_grid={"C": [0.1, 1.0, 10.0], "gamma": [0.001, 0.01, 0.1]},
                       verbose=2)
    # clf = SVR(verbose=2)
    clf.fit(X, Y)
    print(clf.best_params_)
    return clf


def learn_kernel_ridge(X, Y):
    print('learning...')
    kr = GridSearchCV(KernelRidge(kernel='rbf'), verbose=2, cv=5,
                      param_grid={'alpha': [1e0, 0.1, 1e-2, 1e-3], 'gamma': np.logspace(-2, 2, 5)})
    kr.fit(X, Y)
    print(kr.best_params_)
    return kr


def random_forest(X, Y):
    print('learning...')
    rf = GridSearchCV(RandomForestRegressor(), verbose=2, cv=5, param_grid={'n_estimators': [10, 15, 20],
                                                                            'max_depth': [5, 8]})
    # rf = RandomForestRegressor(n_estimators=15, max_depth=6)
    rf.fit(X, Y)
    print(rf.best_params_)
    return rf


def adaboost_regressor(X, Y):
    print('learning')
    ada = GridSearchCV(AdaBoostRegressor(loss='square'), verbose=2, cv=5,
                       param_grid={'learning_rate': [0.01, 0.1, 1.0], 'n_estimators': [40, 50],
                                   'base_estimator': [DecisionTreeRegressor(max_depth=8),
                                                      DecisionTreeRegressor(max_depth=5)]})
    # ada = AdaBoostRegressor(DecisionTreeRegressor(max_depth=3), learning_rate=0.1, n_estimators=10)
    ada.fit(X, Y)
    print(ada.best_params_)
    return ada


def predict(model, Xt, probabilistic=False):
    if probabilistic:
        Yp = model.predict_proba(Xt)
        return np.array(Yp)[:, 1]
    Yp = model.predict(Xt)
    return np.array(Yp).clip(min=0, max=1)


def learn(X, Y, Xt, method=random_forest, use_external_validation=False, Xv=None, Yv=None, probabilistic=False):
    print('Cross validation')
    if not use_external_validation:
        skf = StratifiedKFold(Y, 5)
        model = method(X, Y)
        for train, test in skf:
            # model = method(X[train], Y[train])
            print('predicting...')
            Yp = predict(model, X[test], probabilistic)
            Yt = Y[test]
            score = roc_auc_score(Yt, Yp)
            print('auc for validation set: {}'.format(score))
        # model = method(X, Y)
    else:
        model = method(X, Y)
        Yp = predict(model, Xv)
        Yt = Yv
        score = roc_auc_score(Yt, Yp)
        print('auc for validation set: {}'.format(score))

    Yp = predict(model, Xt, probabilistic)
    print('saving submission...')
    save_submission(Yp)
