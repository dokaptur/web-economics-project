import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

###########
#Read Data#
###########
rawdata = pd.read_csv("/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/data_train.txt", header=None, sep=r"\s+")
rawdata.columns = ['Click', 'Weekday','Hour','Timestamp','Log Type','User ID','User-Agent','IP','Region','City','Ad Exchange','Domain','URL','Anonymous URL ID','Ad slot ID','Ad slot width','Ad slot height','Ad slot visibility','Ad slot format','Ad slot floor price','Creative ID','Key Page URL','Advertiser ID','User Tags']

##########
#CTR MAPS#
##########

###Weekday
weekday_Click = rawdata.groupby('Weekday').apply(lambda x: x['Click'].sum())
weekday_whole = rawdata.groupby('Weekday').size()
weekday_ctr = weekday_Click / weekday_whole 
weekday_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/weekday_ctr.csv')

###Hour
hour_Click = rawdata.groupby('Hour').apply(lambda x: x['Click'].sum())
hour_whole = rawdata.groupby('Hour').size()
hour_ctr = hour_Click / hour_whole 
hour_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/hour_ctr.csv')

###Log Type
rawdata['Log Type'].unique() # check the number of values

###Region
region_Click = rawdata.groupby('Region').apply(lambda x: x['Click'].sum())
region_whole = rawdata.groupby('Region').size()
region_ctr = region_Click / region_whole 
region_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/region_ctr.csv')

###City
city_Click = rawdata.groupby('City').apply(lambda x: x['Click'].sum())
city_whole = rawdata.groupby('City').size()
city_ctr = city_Click / city_whole 
city_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/city_ctr.csv')

###Creative ID
rawdata['Creative ID'].unique()
creativeid_Click = rawdata.groupby('Creative ID').apply(lambda x: x['Click'].sum())
creativeid_whole = rawdata.groupby('Creative ID').size()
creativeid_ctr = creativeid_Click / creativeid_whole 
creativeid_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/creativeid_ctr.csv')

###Key page URL
rawdata['Key Page URL'].unique()
url_Click = rawdata.groupby('Key Page URL').apply(lambda x: x['Click'].sum())
url_whole = rawdata.groupby('Key Page URL').size()
url_ctr = url_Click / url_whole 
url_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/url_ctr.csv')
