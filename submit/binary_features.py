import pandas as pd
import numpy as np
import json


headers = ['click', 'weekday', 'hour', 'timestamp', 'log_type', 'user_id', 'user_agent', 'ip', 'region', 'city',
           'ad_exchange', 'domain', 'url', 'anonymous_url_id', 'ad_slot_id', 'ad_slot_width', 'ad_slot_height',
           'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price', 'creative_id', 'key_page_url',
           'advertiser_id', 'user_tags']

headers_to_use = ['weekday', 'hour', 'region', 'city',  'user_agent', 'ad_exchange', 'ad_slot_width', 'ad_slot_height',
                  'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price', 'creative_id', 'key_page_url']

train_file_prefix = 'data/train'


def load_all_data(file_path='data/shuffle_data_train.txt', test=False):
    with open(file_path, 'r') as data_file:
        names = headers
        if test:
            names = headers[1:]
        data = pd.read_csv(data_file, delimiter='\t', names=names)
        return data


def data_as_dict(row):
    d = {}
    for k in headers_to_use:
        d[k] = str(row[k])

    header = 'user_tags'
    tags = str(row[header]).split(',')
    for tag in tags:
        h = '{}_{}'.format(header, tag)
        d[h] = '1'
    return d


if __name__ == '__main__':
    for nr in range(14):
        data_dict = []
        clicked = []
        print('\nProcessing file number {}...'.format(nr))
        suffix = '{num:02d}'.format(num=nr)
        train_filename = '{}{}'.format(train_file_prefix, suffix)
        train_X_filename = 'data/json/train_X_{}.json'.format(suffix)
        train_Y_filename = 'data/json/train_Y_{}'.format(suffix)
        data = load_all_data(train_filename)
        for i, r in data.iterrows():
            data_dict.append(data_as_dict(r))
            clicked.append(r['click'])
        print('All data processed. Writing features to json...')
        with open(train_X_filename, 'w') as f:
            json.dump(data_dict, f)
        np.save(train_Y_filename, clicked)
        # Xr = load_sparse_csr('{}.{}'.format(train_X_filename, 'npz'))
        # print(v.inverse_transform(Xr))
    print('Finished for training data!')

    print('processing test set...')
    test_filename = 'data/shuffle_data_test.txt'
    data = load_all_data(test_filename, test=True)
    data_dict = []
    for i, r in data.iterrows():
        data_dict.append(data_as_dict(r))
    with open('data/json/test_X.json', 'w') as f:
        json.dump(data_dict, f)
    print('Finished for test data!')

    print('processing positive training set...')
    train_filename = 'data/train_positives'
    data = load_all_data(train_filename)
    data_dict = []
    clicked = []
    for i, r in data.iterrows():
        data_dict.append(data_as_dict(r))
        clicked.append(r['click'])
    print('All data processed. Writing features to json...')
    with open('data/json/train_positives_X.json', 'w') as f:
        json.dump(data_dict, f)
    np.save('data/json/train_positives_Y', clicked)
