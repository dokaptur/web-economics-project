# UCL Web Economics Course Project

This is a source code for UCL Web Economics Project to predict user's click response to an advertisement. It has been created for a private [Kaggle Competition](https://inclass.kaggle.com/c/ucl-web-economics-algorithm-challenge-2016) by XiDoYi team.

## Prerequisits
- Python 3.5
- numpy
- pandas
- scikit-learn
- xgboost

## How to use this code:

1. Clone this repository.
```
git clone https://gitlab.com/dokaptur/web-economics-project.git
```
2. Go to repo root directory and create there 'data' directory. Then create 'json' directory inside 'data'.
```
cd web-economics-project
mkdir data
cd data
mkdir json
cd ..
```
3. Put all data provided by TAs inside data directory.
4. Shuffle training data
```
shuf data_train.txt >> shuffle_data_train.txt
```
5. Split training data to smaller chunks.
```
split -l 200000 -d shuffle_data_train.txt train
```
6. To create binary features' maps, run
```
python binary_features.py
```
7. To create statistical features' maps, run
```
python ctr_features.py
```
8. To create joined binary and statistical features' maps, run
```
python join_features.py
```
9. If you want to reproduce our team's final result, just run
```
python learb_binary.py
```
The predicted probabilities for training set should be generated as 'data/tmp_submission.csv'.
10. If you want to experiment with different models and features, adjust feature file names, number of loaded examples and learning method in "main" function of learn_binary.py script. You can choose from different learning methods implemented in learn.py

Enjoy your experiments!
