import json


def get_unscaled_freq_map(original='data/freq_map.json', output='data/unscaled_freq_map.json'):
    with open(original, 'r') as f:
        freqs = json.load(f)
    for feature in freqs:
        if feature == 'user_tags':
            continue
        num_values = len(freqs[feature])
        for val in freqs[feature]:
            freqs[feature][val] /= num_values
    with open(output, 'w') as f:
        json.dump(freqs, f)


if __name__ == '__main__':
    get_unscaled_freq_map()