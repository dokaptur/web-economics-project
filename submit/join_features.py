import json


def get_file_tuples():
    files_to_use = []
    for nr in range(0, 14, 1):
        suffix = '{num:02d}'.format(num=nr)
        train_X_filename = 'data/json/train_X_{}.json'.format(suffix)
        ctr_train_X_filename = 'data/json/ctr_train_X_{}.json'.format(suffix)
        output_filname = 'data/json/join_train_X_{}.json'.format(suffix)
        files_to_use.append((train_X_filename, ctr_train_X_filename, output_filname))
    files_to_use.append(('data/json/train_positives_X.json', 'data/json/ctr_train_positives_X.json',
                         'data/json/join_train_positives_X.json'))
    files_to_use.append(('data/json/test_X.json', 'data/json/ctr_test_X.json',
                         'data/json/join_test_X.json'))
    return files_to_use


def create_join_dict(bin_dict, ctr_dict):
    for key in ctr_dict:
        if key == 'ctr_ip':
            continue
        assert(key not in bin_dict)
        bin_dict[key] = ctr_dict[key]
    return bin_dict


def create_new_file(bin_filename, ctr_filename, join_filename):
    with open(bin_filename, 'r') as binf:
        bin_dict_list = json.load(binf)
    with open(ctr_filename, 'r') as ctrf:
        ctr_dict_list = json.load(ctrf)

    join_dict_list = []
    for bin_dict, ctr_dict in zip(bin_dict_list, ctr_dict_list):
        join_dict_list.append(create_join_dict(bin_dict, ctr_dict))

    with open(join_filename, 'w') as f:
        json.dump(join_dict_list, f)


if __name__ == '__main__':
    file_tuples = get_file_tuples()
    for bin_filename, ctr_filename, join_filename in file_tuples:
        print('processing file {}'.format(join_filename))
        create_new_file(bin_filename, ctr_filename, join_filename)


