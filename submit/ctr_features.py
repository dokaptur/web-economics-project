import numpy as np
import pandas as pd
import os
import json

headers = ['click', 'weekday', 'hour', 'timestamp', 'log_type', 'user_id', 'user_agent', 'ip', 'region', 'city',
           'ad_exchange', 'domain', 'url', 'anonymous_url_id', 'ad_slot_id', 'ad_slot_width', 'ad_slot_height',
           'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price', 'creative_id', 'key_page_url',
           'advertiser_id', 'user_tags']

headers_to_use = ['weekday', 'hour', 'user_agent', 'ip', 'region', 'city', 'ad_exchange', 'domain', 'ad_slot_id',
                  'ad_slot_width', 'ad_slot_height', 'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price',
                  'creative_id', 'key_page_url']

train_file_prefix = 'data/train'


def load_all_data(file_path='data/shuffle_data_train.txt', test=False):
    with open(file_path, 'r') as data_file:
        names = headers
        if test:
            names = headers[1:]
        data = pd.read_csv(data_file, delimiter='\t', names=names)
        return data


def create_ctr_map():
    ctr_map = {}
    dirpath = 'data/ctr'
    for ctrfilename in os.listdir(dirpath):
        if ctrfilename.endswith('.csv'):
            print('processing file {}'.format(ctrfilename))
            ctrfile = os.path.join(dirpath, ctrfilename)
            df = pd.read_csv(ctrfile)
            feature_name = list(df.columns.values)[0]
            print('processing feature {}\n'.format(feature_name))
            ctr_map[feature_name] = {}
            for r in df.itertuples(index=False):
                ctr_map[feature_name][str(r[0])] = r[-1]  # * 1000  # if we want to scale
    print('creating map finished\nsaving to json')
    with open('data/ctr/ctr_map.json', 'w') as f:
        json.dump(ctr_map, f)


def data_as_dict(ctr_map, freq_map, row, must_appear=True):
    d = {}

    # plain features
    for header in headers_to_use:
        key = str(row[header])
        if not header == 'ip':
            assert((not must_appear) or (key in freq_map[header]))
            d['freq_{}'.format(header)] = freq_map[header].get(key, 0.0)
        assert((not must_appear) or (key in ctr_map[header]))
        d['ctr_{}'.format(header)] = ctr_map[header].get(key, 0.0)

    # ad slot area feature
    header = 'ad_slot_area'
    key = str(row['ad_slot_width'] * row['ad_slot_height'])
    assert((not must_appear) or (key in ctr_map[header]))
    d['ctr_{}'.format(header)] = ctr_map[header].get(key, 0.0)

    # user tag feature. get mean of user tags ctr
    header = 'user_tags'
    keys = str(row[header]).split(',')
    assert(len(keys) > 0)
    ctr_sum = 0.0
    freq_sum = 0.0
    for key in keys:
        assert((not must_appear) or (key in ctr_map[header]))
        assert((not must_appear) or (key in freq_map[header]))
        ctr_sum += ctr_map[header].get(key, 0.0)
        freq_sum += freq_map[header].get(key, 0.0)
    d['ctr_{}'.format(header)] = ctr_sum / len(keys)
    d['freq_{}'.format(header)] = freq_sum / len(keys)

    return d


if __name__ == '__main__':
    create_ctr_map()
    with open('data/ctr/ctr_map.json', 'r') as f:
        ctr_map = json.load(f)
    with open('data/unscaled_freq_map.json') as f:
        freq_map = json.load(f)
    print('ctr ad freq maps loaded!')
    for nr in range(14):
        data_dict = []
        clicked = []
        print('\nProcessing file number {}...'.format(nr))
        suffix = '{num:02d}'.format(num=nr)
        train_filename = '{}{}'.format(train_file_prefix, suffix)
        train_X_filename = 'data/json/ctr_train_X_{}.json'.format(suffix)
        train_Y_filename = 'data/json/ctr_train_Y_{}'.format(suffix)
        data = load_all_data(train_filename)
        for i, r in data.iterrows():
            data_dict.append(data_as_dict(ctr_map, freq_map, r))
            clicked.append(r['click'])
        print('All data processed. Writing features to json...')
        with open(train_X_filename, 'w') as f:
            json.dump(data_dict, f)
        np.save(train_Y_filename, clicked)
    print('Finished for training data!')

    print('processing test set...')
    test_filename = 'data/shuffle_data_test.txt'
    data = load_all_data(test_filename, test=True)
    data_dict = []
    for i, r in data.iterrows():
        data_dict.append(data_as_dict(ctr_map, freq_map, r, must_appear=False))
    with open('data/json/ctr_test_X.json', 'w') as f:
        json.dump(data_dict, f)
    print('Finished for test data!')

    print('processing positive training set...')
    train_filename = 'data/train_positives'
    data = load_all_data(train_filename)
    data_dict = []
    clicked = []
    for i, r in data.iterrows():
        data_dict.append(data_as_dict(ctr_map, freq_map, r))
        clicked.append(r['click'])
    print('All data processed. Writing features to json...')
    with open('data/json/ctr_train_positives_X.json', 'w') as f:
        json.dump(data_dict, f)
    np.save('data/json/ctr_train_positives_Y', clicked)
