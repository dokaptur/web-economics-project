import pandas as pd
import json

headers = ['click', 'weekday', 'hour', 'timestamp', 'log_type', 'user_id', 'user_agent', 'ip', 'region', 'city',
           'ad_exchange', 'domain', 'url', 'anonymous_url_id', 'ad_slot_id', 'ad_slot_width', 'ad_slot_height',
           'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price', 'creative_id', 'key_page_url',
           'advertiser_id', 'user_tags']

headers_to_use = ['weekday', 'hour', 'user_agent', 'region', 'city', 'ad_exchange', 'domain', 'ad_slot_id',
                  'ad_slot_width', 'ad_slot_height', 'ad_slot_visibility', 'ad_slot_format', 'ad_slot_floor_price',
                  'creative_id', 'key_page_url']


def load_file(filename):
    data = pd.read_csv(filename, delimiter='\t', names=headers)
    return data


def get_count_map(data, header):
    uniqs = data[header].unique()
    d = {}
    for val in uniqs:
        d[str(val)] = (data[data[header] == val].shape[0] / data.shape[0]) * len(uniqs)
    return d


def get_user_tag_map(data):
    utags = data['user_tags']
    count_map = {}
    for ut in utags:
        tags = str(ut).split(',')
        for tag in tags:
            if tag not in count_map:
                count_map[tag] = 0
            count_map[tag] += 1
    for tag in count_map:
        count_map[tag] /= data.shape[0]
    return count_map


def create_freq_map(data, outputfile='data/freq_map.json'):
    d = {}
    for h in headers_to_use:
        d[h] = get_count_map(data, h)
    d['user_tags'] = get_user_tag_map(data)
    with open(outputfile, 'w') as f:
        json.dump(d, f)


if __name__ == '__main__':
    filename = 'data/shuffle_data_train.txt'
    data = load_file(filename)
    create_freq_map(data)
