import numpy as np
import pandas as pd
import matplotlib.pyplot as plt


rawdata = pd.read_csv("/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/data_train.txt", header=None, sep=r"\s+")
rawdata.columns = ['Click', 'Weekday','Hour','Timestamp','Log Type','User ID','User-Agent','IP','Region','City','Ad Exchange','Domain','URL','Anonymous URL ID','Ad slot ID','Ad slot width','Ad slot height','Ad slot visibility','Ad slot format','Ad slot floor price','Creative ID','Key Page URL','Advertiser ID','User Tags']


#split each column of tags to list
usertag_list = rawdata['User Tags'].str.split(',').tolist() 

#get the total tags
x = []
for i in usertag_list:
    l = len(i)
    for j in range(0,l):
        if i[j] not in x:
            x.append(i[j])

# get tags and relative index. in the dataframe 'tag_position', t represents tags and p means index in rawdata.
m = len(usertag_list)
tags = []
for i in range(0,m):
    for j in usertag_list[i]:
        tags.append([j,i])
tag_position = pd.DataFrame(tags, columns=list('tp'))

# get the corresponding 'Click' according to the index. Since the data set is really huge, then divided data.
tag_clickrecord = []
for i in range(0,1000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(1000000,2000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(2000000,3000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(3000000,4000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(4000000,5000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(5000000,6000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(6000000,7000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(7000000,8000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(8000000,9000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(9000000,10000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(10000000,11000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(11000000,12000000):
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)
for i in range(12000000,12034785): # the length of tags_position is 12034785
    p = tag_position.ix[i,1]
    value = rawdata.ix[p,0]
    tag_clickrecord.append(value)

tag_position['Click'] = tag_clickrecord
usertag_Click = tag_position.groupby('t').apply(lambda x: x['Click'].sum())
usertag_whole = tag_position.groupby('t').size()
usertag_ctr = usertag_Click / usertag_whole 
usertag_ctr.to_csv('/Users/yangyijing/Desktop/Exam_Study/UCL_COURSEWORKS/web economics/groupwork/web-economics-project/data/CTRMAP/usertag_ctr.csv')


