from learn import *
from sklearn.feature_extraction import DictVectorizer

if __name__ == '__main__':
    files_to_use = []
    for nr in range(0, 2, 1):
        suffix = '{num:02d}'.format(num=nr)
        train_X_filename = 'data/json/train_X_{}.json'.format(suffix)
        train_Y_filename = 'data/json/train_Y_{}.npy'.format(suffix)
        files_to_use.append((train_X_filename, train_Y_filename))
    files_to_use.append(('data/json/train_positives_X.json', 'data/json/train_positives_Y.npy'))
    X_dict = []
    Y = np.array([])
    for train_X_filename, train_Y_filename in files_to_use:
        print('loading files {} and {}...'.format(train_X_filename, train_Y_filename))
        X_dict_chunk, Y_chunk = load_data(train_X_filename, train_Y_filename)
        X_dict += X_dict_chunk[:min(len(X_dict_chunk), 15000)]
        Y = np.hstack((Y, Y_chunk[:min(len(X_dict_chunk), 15000)]))

    print('loading test file...')
    Xt_dict = load_json('data/json/test_X.json')
    # val_X_filename = 'data/json/join_train_X_{}.json'.format(11)
    # val_Y_filename = 'data/json/train_Y_{}.npy'.format(11)
    # Xv_dict, Yv = load_data(val_X_filename, val_Y_filename)

    print('creating sparse representation...')
    dv = DictVectorizer(sparse=True)
    dv.fit(X_dict)
    X = dv.transform(X_dict)

    print('shape of training data: {}'.format(X.shape))
    Xt = dv.transform(Xt_dict)
    print('shape of test data: {}'.format(Xt.shape))
    # Xv = dv.transform(Xv_dict)
    # print('shape of validation data: {}'.format(Xv.shape))

    learn(X, Y, Xt, method=learn_xgboost, probabilistic=True)